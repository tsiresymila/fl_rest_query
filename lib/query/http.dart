typedef QueryTransformer = dynamic Function(dynamic data);

enum Method { get, post, put, delete, patch, head, options }

class RestBaseQueryParams {
  final String tag;
  final String url;

  final Method method;

  final Map<String, dynamic>? query;
  final Map<String, dynamic>? params;
  final Map<String, dynamic>? headers;
  final List<QueryTransformer> transformers;

  RestBaseQueryParams(
      {required this.tag,
      required this.url,
      required this.method,
      this.query,
      this.params,
      this.headers,
      this.transformers = const []});
}

abstract class HttpBaseQuery {
  performQuery<ParamType extends RestBaseQueryParams, ResponseType>(
          ParamType params) =>
      ResponseType;

  performMutation<ParamType extends RestBaseQueryParams, DataType,
          ResponseType>(ParamType params, DataType data) async =>
      Future<ResponseType>;
}
