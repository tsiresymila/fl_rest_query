import 'package:fl_query/fl_query.dart';
import 'package:flutter/material.dart';

import 'http.dart';

class RestQueryClient extends InheritedWidget {
  final HttpBaseQuery baseQuery;
  final Function(dynamic error, dynamic raison)? onError;

  const RestQueryClient(
      {super.key, required this.baseQuery, required super.child, this.onError});

  static RestQueryClient? of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<RestQueryClient>();

  @override
  bool updateShouldNotify(RestQueryClient oldWidget) {
    return true;
  }
}

class RestQueryClientProvider extends StatefulWidget {
  final HttpBaseQuery baseQuery;
  final Widget child;
  final QueryClient? client;
  final QueryCache? cache;
  final Duration cacheDuration;
  final int? maxRetries;
  final Duration? retryDelay;
  final Duration? staleDuration;
  final Duration? refreshInterval;
  final bool? refreshOnMount;
  final bool? refreshOnQueryFnChange;
  final Function(dynamic error, dynamic raison)? onError;

  const RestQueryClientProvider(
      {super.key,
      required this.baseQuery,
      required this.child,
      this.client,
      this.cache,
      this.cacheDuration = DefaultConstants.cacheDuration,
      this.retryDelay,
      this.staleDuration,
      this.refreshInterval,
      this.maxRetries,
      this.refreshOnMount,
      this.onError,
      this.refreshOnQueryFnChange});

  @override
  State<RestQueryClientProvider> createState() => _RestQueryProviderState();
}

class _RestQueryProviderState extends State<RestQueryClientProvider> {
  @override
  Widget build(BuildContext context) {
    return QueryClientProvider(
        client: widget.client,
        cache: widget.cache,
        cacheDuration: widget.cacheDuration,
        maxRetries: widget.maxRetries,
        retryDelay: widget.retryDelay,
        staleDuration: widget.staleDuration,
        refreshInterval: widget.refreshInterval,
        refreshOnMount: widget.refreshOnMount,
        refreshOnQueryFnChange: widget.refreshOnQueryFnChange,
        child: RestQueryClient(
          baseQuery: widget.baseQuery,
          onError: widget.onError,
          child: widget.child,
        ));
  }
}
