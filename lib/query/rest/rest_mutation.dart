import 'package:fl_query/fl_query.dart';
import 'package:flutter/material.dart';

import '../provider.dart';
import '../http.dart';

class RestMutationBuilder<ResponseType, QueryType extends RestBaseQueryParams,
        ErrorType, VariablesType, RecoveryType>
    extends MutationBuilder<ResponseType, ErrorType, VariablesType,
        RecoveryType> {
  final HttpBaseQuery? baseQuery;
  final QueryType params;

  RestMutationBuilder({
    super.key,
    required BuildContext context,
    this.baseQuery,
    required this.params,
    required Widget Function(BuildContext, Mutation) builder,
    super.retryConfig,
    super.onData,
    Function(ErrorType, RecoveryType?)? onError,
    super.onMutate,
    List<String>? invalidateQueryTags,
    List<String>? invalidateInfiniteQueryTag,
  }) : super(
            params.tag,
            (data) async {
              return await (RestQueryClient.of(context)!.baseQuery)
                  .performMutation<QueryType, VariablesType, ResponseType>(
                      params, data);
            },
            refreshQueries: invalidateQueryTags,
            refreshInfiniteQueries: invalidateInfiniteQueryTag,
            builder: builder,
            onError: (error, v) {
              var onErrorGlobal = RestQueryClient.of(context)!.onError;
              if (onErrorGlobal != null) onErrorGlobal(error, v);
              if (onError != null) onError(error, v);
            });
}
