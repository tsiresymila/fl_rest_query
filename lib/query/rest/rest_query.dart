import 'package:fl_query/fl_query.dart';
import 'package:flutter/material.dart';

import '../http.dart';
import '../provider.dart';

class RestQueryBuilder<DataType, QueryType extends RestBaseQueryParams,
    ErrorType> extends QueryBuilder<DataType, ErrorType> {
  final QueryType params;

  RestQueryBuilder({
    super.key,
    required BuildContext context,
    super.initial,
    super.retryConfig,
    super.refreshConfig,
    Function(dynamic data)? onData,
    Function(ErrorType)? onError,
    super.enabled = true,
    required Widget Function(BuildContext, Query) builder,
    required this.params,
  }) : super(
            params.tag,
            () async {
              return await (RestQueryClient.of(context)!.baseQuery)
                  .performQuery<QueryType, DataType>(params);
            },
            builder: builder,
            onData: (data) {
              if (onData != null) onData(data);
            },
            jsonConfig: JsonConfig(
              fromJson: (json) {
                var data = json['data'];
                for (var trans in params.transformers) {
                  data = trans(data);
                }
                return data;
              },
              toJson: (data) {
                return {'data': data};
              },
            ),
            onError: (error) {
              // dispatch bloc event

              var onErrorGlobal = RestQueryClient.of(context)!.onError;
              if (onErrorGlobal != null) onErrorGlobal(error, null);
              if (onError != null) onError(error);
            });
}
