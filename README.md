# Flutter Rest Query packages

## Get started

Add **fl_rest_query** and **fl_query** in you depandancies.

## Usage

1 - Create your http base query. For example with dio

```dart
import 'package:fl_rest_query/fl_rest_query.dart';
import 'package:dio/dio.dart' as dio;
import 'package:flutter/material.dart';

class HttpDio implements HttpBaseQuery {
  late final dio.Dio req;

  final BuildContext context;

  HttpDio(this.context, {dio.Dio? dioOverride}) {
    req = dioOverride ??
        dio.Dio(
            dio.BaseOptions(baseUrl: 'https://jsonplaceholder.typicode.com'));
  }

  @override
  performMutation<T extends RestBaseQueryParams, DataType, Response>(
      params, data) async {
    var response = await req.request(params.url,
        data: data,
        queryParameters: params.params, onSendProgress: (received, total) {
    },
        options: dio.Options(
          headers: params.headers,
          method: params.method.name,
        ));
    return response.data;
  }

  @override
  performQuery<T extends RestBaseQueryParams, Response>(params) async {
    var response = await req.request(params.url, queryParameters: params.params,
        onReceiveProgress: (received, total) {
    },
        options: dio.Options(
          headers: params.headers,
          method: params.method.name,
        ));
    return response.data as Response;
  }
}

```

2 - Initilize configuration like fl_query
<br>In `main.dart`.

```dart

import 'package:example/page.dart';
import 'package:fl_query/fl_query.dart';
import 'package:fl_rest_query/fl_rest_query.dart';
import 'package:flutter/material.dart';

import 'http_dio.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await QueryClient.initialize(cachePrefix: 'fl_rest_query');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return RestQueryClientProvider( // <- use this instead of QueryClientProvider from fl_query
      baseQuery: HttpDio(context), // <- use the base query
      client: QueryClient(),
      child: MaterialApp(
        title: 'Rest Query',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            useMaterial3: true,
            primarySwatch: Colors.cyan,
            primaryColor: Colors.cyan),
        home: const HomePage(),
      ),
    );
  }
}


```

3 - Use restMutationBuilder and RestQueryBuilder in your code.

```dart
// import 'package:example/log.dart';
import 'package:fl_query/fl_query.dart';
import 'package:fl_rest_query/fl_rest_query.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            RestQueryBuilder(
              context: context,
              params: RestBaseQueryParams(
                  method: Method.GET, tag: 'GET-POST', url: '/posts'),
              builder: (context, Query query) {
                if (query.isLoading) {
                  return const CupertinoActivityIndicator();
                }

                if (query.hasError) {
                  return Text(query.error.toString());
                }
                // log.e(query.data);
                return RestMutationBuilder(
                    context: context,
                    params: RestBaseQueryParams(
                        method: Method.POST, tag: 'ADD-POST', url: '/posts'),
                    onData: (data, d) {
                      // log.i(data);
                    },
                    builder: (context, mutation) {
                      return ElevatedButton(
                          onPressed: () {
                            mutation.mutate({
                              "title": 'foo',
                              "body": 'bar',
                              "userId": 1,
                            });
                          },
                          child: const Text('Add Post'));
                    });
              },
            )
          ],
        ),
      ),
    );
  }
}

```

## Additional information

This package depends on **fl_query** with **1.0.0-alpha.2** version. So make sure that you add fl_query in in your package depandancies

## Stay in touch

Author: [Tsiresy Mila ](tsiresymila@gmail.com)
